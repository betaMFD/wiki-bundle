<?php

namespace BetaMFD\WikiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="wiki_post")
 * @ORM\Entity(repositoryClass="\BetaMFD\WikiBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\PostType")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @var integer
     *
     * I've found that this has to be nullable so that it can save then update with the new ID
     *
     * @ORM\OneToOne(targetEntity="BetaMFD\WikiBundle\Entity\PostVersion", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id", nullable=true)
     */
    private $currentVersion;

    /**
     * @var integer
     *
     * If the slug changes, the old slug should redirect to the new one, so this can signal that
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\Post")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $redirect;

    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="BetaMFD\WikiBundle\Entity\Tag", inversedBy="posts", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="wiki_posts_tags")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\WikiBundle\Entity\PostVersion",
     *  mappedBy="post", cascade={"persist"})
     */
    private $versions;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $topCurrentVersion = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $topMajor = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $topMinor = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $topPatch = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $topMeta;

    /**
     * This is to try to prevent race conditions
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    protected $changeVersion;

    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->versions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getCurrentVersion()->getTitle();
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public function getSemanticVersion()
    {
        $version = $this->topMajor . '.' . $this->topMinor . '.' . $this->topPatch;
        if (!empty($this->topMeta)) {
            $version .= '-' . $this->topMeta;
        }
        return $version;
    }

    public function setNewMajorVersion(PostVersion $version, $meta = null)
    {
        $this->setNewVersion($version);
        $major = ++$this->topMajor;
        $version->setMajor($major);
        $this->topMinor = 0;
        $version->setMinor(0);
        $this->topPatch = 0;
        $version->setPatch(0);
        $this->topMeta = $meta;
        $version->setMeta($meta);
    }

    public function setNewMinorVersion(PostVersion $version, $meta = null)
    {
        $this->setNewVersion($version);
        $version->setMajor($this->topMajor);
        $minor = ++$this->topMinor;
        $version->setMinor($minor);
        $this->topPatch = 0;
        $version->setPatch(0);
        $this->topMeta = $meta;
        $version->setMeta($meta);
    }

    public function setNewPatchVersion(PostVersion $version, $meta = null)
    {
        $this->setNewVersion($version);
        $version->setMajor($this->topMajor);
        $version->setMinor($this->topMinor);
        $patch = ++$this->topPatch;
        $version->setPatch($patch);
        $this->topMeta = $meta;
        $version->setMeta($meta);
    }

    public function setNewMetaVersion(PostVersion $version, $meta = null)
    {
        $this->setNewVersion($version);
        $version->setMajor($this->topMajor);
        $version->setMinor($this->topMinor);
        $version->setPatch($this->topPatch);
        $this->topMeta = $meta;
        $version->setMeta($meta);
    }

    private function setNewVersion(PostVersion $version)
    {
        $this->addVersion($version);
        $this->setCurrentVersion($version);
        $topV = ++$this->topCurrentVersion;
        $version->setCurrentVersion($topV);
        $version->setPost($this);
    }

    #########################################################################
    #                            Getters/Setters                            #
    #########################################################################

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of Type
     *
     * @param integer type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of Slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set the value of Slug
     *
     * @param string slug
     *
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the value of CurrentVersion
     *
     * @return integer
     */
    public function getCurrentVersion()
    {
        return $this->currentVersion;
    }

    /**
     * Get the value of CurrentVersion
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->currentVersion;
    }

    /**
     * Set the value of CurrentVersion
     *
     * @param integer version
     *
     * @return self
     */
    public function setCurrentVersion($version)
    {
        $this->currentVersion = $version;

        return $this;
    }

    /**
     * Get the value of Tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Implodes tags into a string for form editing
     *
     * @return string $tag_list
     */
    public function getTagString()
    {
        $tags = $this->getTags();
        if (!empty($tags)) {
            $tags = $tags->toArray();
            $tag_list = implode(', ', $tags);
            return $tag_list;
        }
        return '';
    }

    /**
     * Resets the $tags ArrayCollection
     *
     * @return $this
     */
    public function clearTags()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();

        return $this;
    }

    /**
     * Add tag
     *
     * @param BetaMFD\WikiBundle\Entity\Tag $tag
     *
     * @return Tag
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove Tag
     *
     * @param BetaMFD\WikiBundle\Entity\Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get the value of Versions
     *
     * @return mixed
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * Add version
     *
     * @param BetaMFD\WikiBundle\Entity\PostVersion $version
     *
     * @return self
     */
    public function addVersion(PostVersion $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove Version
     *
     * @param BetaMFD\WikiBundle\Entity\PostVersion $version
     */
    public function removeVersion(PostVersion $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get the value of Redirect
     *
     * @return integer
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * Set the value of Redirect
     *
     * @param integer redirect
     *
     * @return self
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;

        return $this;
    }

    /**
     * Get the value of Top Current Version
     *
     * @return integer
     */
    public function getTopCurrentVersion()
    {
        return $this->topCurrentVersion;
    }

    /**
     * Set the value of Top Current Version
     *
     * @param integer topCurrentVersion
     *
     * @return self
     */
    public function setTopCurrentVersion($topCurrentVersion)
    {
        $this->topCurrentVersion = $topCurrentVersion;

        return $this;
    }

    /**
     * Get the value of Top Major
     *
     * @return integer
     */
    public function getTopMajor()
    {
        return $this->topMajor;
    }

    /**
     * Set the value of Top Major
     *
     * @param integer topMajor
     *
     * @return self
     */
    public function setTopMajor($topMajor)
    {
        $this->topMajor = $topMajor;

        return $this;
    }

    /**
     * Get the value of Top Minor
     *
     * @return integer
     */
    public function getTopMinor()
    {
        return $this->topMinor;
    }

    /**
     * Set the value of Top Minor
     *
     * @param integer topMinor
     *
     * @return self
     */
    public function setTopMinor($topMinor)
    {
        $this->topMinor = $topMinor;

        return $this;
    }

    /**
     * Get the value of Top Patch
     *
     * @return integer
     */
    public function getTopPatch()
    {
        return $this->topPatch;
    }

    /**
     * Set the value of Top Patch
     *
     * @param integer topPatch
     *
     * @return self
     */
    public function setTopPatch($topPatch)
    {
        $this->topPatch = $topPatch;

        return $this;
    }

    /**
     * Get the value of Top Meta
     *
     * @return string
     */
    public function getTopMeta()
    {
        return $this->topMeta;
    }

    /**
     * Set the value of Top Meta
     *
     * @param string topMeta
     *
     * @return self
     */
    public function setTopMeta($topMeta)
    {
        $this->topMeta = $topMeta;

        return $this;
    }

    /**
     * Get the value of This is to try to prevent race conditions
     *
     * @return integer
     */
    public function getChangeVersion()
    {
        return $this->changeVersion;
    }

    /**
     * Set the value of This is to try to prevent race conditions
     *
     * @param integer changeVersion
     *
     * @return self
     */
    public function setChangeVersion($changeVersion)
    {
        $this->changeVersion = $changeVersion;

        return $this;
    }

    ##########################################################################
    #                              Post Version                              #
    ##########################################################################

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->currentVersion->getTitle();
    }

    /**
     * Set the value of Title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->currentVersion->setTitle($title);

        return $this;
    }

    /**
     * Get the value of Version
     *
     * @return integer
     */
    public function getVersionNumber()
    {
        return $this->currentVersion->getVersion();
    }

    /**
     * Set the value of Version
     *
     * @param integer version
     *
     * @return self
     */
    public function setVersionNumber($version)
    {
        $this->currentVersion->setVersion($version);

        return $this;
    }

    /**
     * Get the value of Previous
     *
     * @return integer
     */
    public function getPrevious()
    {
        return $this->currentVersion->getPrevious();
    }

    /**
     * Set the value of Previous
     *
     * @param integer previous
     *
     * @return self
     */
    public function setPrevious($previous)
    {
        $this->currentVersion->setPrevious($previous);

        return $this;
    }

    /**
     * Get the value of Letter Rev
     *
     * @return string
     */
    public function getLetterRev()
    {
        return $this->currentVersion->getLetterRev();
    }

    /**
     * Set the value of Letter Rev
     *
     * @param string letterRev
     *
     * @return self
     */
    public function setLetterRev($letterRev)
    {
        $this->currentVersion->setLetterRev($letterRev);

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->currentVersion->getDate();
    }

    /**
     * Set the value of Date
     *
     * @param \DateTime date
     *
     * @return self
     */
    public function setDate(\DateTime $date)
    {
        $this->currentVersion->setDate($date);

        return $this;
    }

    /**
     * Get the value of User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->currentVersion->getUser();
    }

    /**
     * Set the value of User
     *
     * @param User user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->currentVersion->setUser($user);

        return $this;
    }

    /**
     * Get the value of Ip
     *
     * @return integer
     */
    public function getIp()
    {
        return $this->currentVersion->getIp();
    }

    /**
     * Set the value of Ip
     *
     * @param integer ip
     *
     * @return self
     */
    public function setIp($ip)
    {
        $this->currentVersion->setIp($ip);

        return $this;
    }

    /**
     * Get the value of Comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->currentVersion->getComment();
    }

    /**
     * Set the value of Comment
     *
     * @param string comment
     *
     * @return self
     */
    public function setComment($comment)
    {
        $this->currentVersion->setComment($comment);

        return $this;
    }

    /**
     * Get the value of Html
     *
     * @return mixed
     */
    public function getHtml()
    {
        return $this->currentVersion->getHtml();
    }

    /**
     * Set the value of Html
     *
     * @param mixed html
     *
     * @return self
     */
    public function setHtml($html)
    {
        $this->currentVersion->setHtml($html);

        return $this;
    }

    /**
     * Get the value of Markup
     *
     * @return mixed
     */
    public function getMarkup()
    {
        return $this->currentVersion->getMarkup();
    }

    /**
     * Set the value of Markup
     *
     * @param mixed markup
     *
     * @return self
     */
    public function setMarkup($markup)
    {
        $this->currentVersion->setMarkup($markup);

        return $this;
    }

    /**
     * Get the value of Approval
     *
     * @return integer
     */
    public function getApproval()
    {
        return $this->currentVersion->getApproval();
    }

    /**
     * Set the value of Approval
     *
     * @param integer approval
     *
     * @return self
     */
    public function setApproval($approval)
    {
        $this->currentVersion->setApproval($approval);

        return $this;
    }

    /**
     * Get the value of Draft
     *
     * @return boolean
     */
    public function isDraft()
    {
        return $this->currentVersion->getDraft();
    }

    /**
     * Set the value of Draft
     *
     * @param boolean draft
     *
     * @return self
     */
    public function setDraft($draft)
    {
        $this->currentVersion->setDraft($draft);

        return $this;
    }

    /**
     * Get the value of Category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->currentVersion->getCategory();
    }

    /**
     * Set the value of Category
     *
     * @param integer category
     *
     * @return self
     */
    public function setCategory($category)
    {
        $this->currentVersion->setCategory($category);

        return $this;
    }

    /**
     * Get the value of User Name
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->currentVersion->getUserName();
    }

    /**
     * Set the value of User Name
     *
     * @param string userName
     *
     * @return self
     */
    public function setUserName($userName)
    {
        $this->currentVersion->setUserName($userName);

        return $this;
    }
}
