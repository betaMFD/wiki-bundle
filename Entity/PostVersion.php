<?php

namespace BetaMFD\WikiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostVersion
 *
 * @ORM\Table(name="wiki_post_version")
 * @ORM\Entity
 */
class PostVersion
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * I've found that this has to be nullable so that it can save then update with the new ID
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\Post",
     *     inversedBy="versions",
     *     cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $post;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=false)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\PostCategory")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currentVersion = 1;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $major = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minor = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $patch = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $meta;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\PostVersion")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $previous;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var UserInterface
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Model\UserInterface")
     */
    private $user;

    /**
     * @var string
     *
     * In case the user is deleted or changes their name.
     * Or something.
     * Keep the record of who they were.
     *
     * @ORM\Column(type="string", length=500, nullable=false)
     */
    private $userName;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $html;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $markup;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\PostApproval")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $approval; //nullable!

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $draft = true;


    public function __construct()
    {
        if (!empty($_SERVER['REMOTE_ADDR'])) {
            $this->ip = ip2long($_SERVER['REMOTE_ADDR']);
        }
        $this->date = new \DateTime;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    ###########################################################################
    #                             Getters/Setters                             #
    ###########################################################################


    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Post
     *
     * @return integer
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set the value of Post
     *
     * @param integer post
     *
     * @return self
     */
    public function setPost($post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set the value of Version
     *
     * @param integer version
     *
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get the value of Previous
     *
     * @return integer
     */
    public function getPrevious()
    {
        return $this->previous;
    }

    /**
     * Set the value of Previous
     *
     * @param integer previous
     *
     * @return self
     */
    public function setPrevious($previous)
    {
        $this->previous = $previous;

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param \DateTime date
     *
     * @return self
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of User
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of User
     *
     * @param User user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;
        $this->userName = $user->getName();

        return $this;
    }

    /**
     * Get the value of Ip
     *
     * @return integer
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set the value of Ip
     *
     * @param integer ip
     *
     * @return self
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get the value of Comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set the value of Comment
     *
     * @param string comment
     *
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get the value of Html
     *
     * @return mixed
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Set the value of Html
     *
     * @param mixed html
     *
     * @return self
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Get the value of Markup
     *
     * @return mixed
     */
    public function getMarkup()
    {
        return $this->markup;
    }

    /**
     * Set the value of Markup
     *
     * @param mixed markup
     *
     * @return self
     */
    public function setMarkup($markup)
    {
        $this->markup = $markup;

        return $this;
    }

    /**
     * Get the value of Approval
     *
     * @return integer
     */
    public function getApproval()
    {
        return $this->approval;
    }

    /**
     * Set the value of Approval
     *
     * @param integer approval
     *
     * @return self
     */
    public function setApproval($approval)
    {
        $this->approval = $approval;

        return $this;
    }

    /**
     * Get the value of Draft
     *
     * @return boolean
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * Set the value of Draft
     *
     * @param boolean draft
     *
     * @return self
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;

        return $this;
    }

    /**
     * Get the value of Category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of Category
     *
     * @param integer category
     *
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get the value of User Name
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set the value of User Name
     *
     * @param string userName
     *
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get the value of Current Version
     *
     * @return integer
     */
    public function getCurrentVersion()
    {
        return $this->currentVersion;
    }

    /**
     * Set the value of Current Version
     *
     * @param integer currentVersion
     *
     * @return self
     */
    public function setCurrentVersion($currentVersion)
    {
        $this->currentVersion = $currentVersion;

        return $this;
    }

    /**
     * Get the value of Major
     *
     * @return integer
     */
    public function getMajor()
    {
        return $this->major;
    }

    /**
     * Set the value of Major
     *
     * @param integer major
     *
     * @return self
     */
    public function setMajor($major)
    {
        $this->major = $major;

        return $this;
    }

    /**
     * Get the value of Minor
     *
     * @return integer
     */
    public function getMinor()
    {
        return $this->minor;
    }

    /**
     * Set the value of Minor
     *
     * @param integer minor
     *
     * @return self
     */
    public function setMinor($minor)
    {
        $this->minor = $minor;

        return $this;
    }

    /**
     * Get the value of Patch
     *
     * @return integer
     */
    public function getPatch()
    {
        return $this->patch;
    }

    /**
     * Set the value of Patch
     *
     * @param integer patch
     *
     * @return self
     */
    public function setPatch($patch)
    {
        $this->patch = $patch;

        return $this;
    }

    /**
     * Get the value of Meta
     *
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set the value of Meta
     *
     * @param string meta
     *
     * @return self
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

}
