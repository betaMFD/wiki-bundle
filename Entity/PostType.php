<?php

namespace BetaMFD\WikiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostType
 *
 * @ORM\Table(name="wiki_post_type")
 * @ORM\Entity
 */
class PostType
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $needApproval = 0;


   /**
    * Get the value of Type
    *
    * @return string
    */
    public function __toString()
    {
        return $this->type;
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of Type
     *
     * @param string type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of Need Approval
     *
     * @return boolean
     */
    public function getNeedApproval()
    {
        return $this->needApproval;
    }

    /**
     * Set the value of Need Approval
     *
     * @param boolean needApproval
     *
     * @return self
     */
    public function setNeedApproval($needApproval)
    {
        $this->needApproval = $needApproval;

        return $this;
    }

}
