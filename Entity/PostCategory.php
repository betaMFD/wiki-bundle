<?php

namespace BetaMFD\WikiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostCategory
 *
 * @ORM\Table(name="wiki_post_category")
 * @ORM\Entity(repositoryClass="\BetaMFD\WikiBundle\Repository\PostCategoryRepository")
 */
class PostCategory
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\WikiBundle\Entity\PostCategory")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $parent;


   /**
    * Get the value of Type
    *
    * @return string
    */
    public function __toString()
    {
        return $this->category;
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of Category
     *
     * @param string category
     *
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get the value of Parent
     *
     * @return integer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set the value of Parent
     *
     * @param integer parent
     *
     * @return self
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

}
