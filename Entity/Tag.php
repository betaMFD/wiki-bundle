<?php

namespace BetaMFD\WikiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table(name="wiki_tag")
 * @ORM\Entity()
 */
class Tag
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="BetaMFD\WikiBundle\Entity\Post", mappedBy="tags")
     */
    private $posts;


    public function __construct($tag)
    {
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->id = $tag;
    }

    public function __toString()
    {
        return $this->id;
    }


    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getTag()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setTag($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Posts
     *
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Add post
     *
     * @param BetaMFD\WikiBundle\Entity\Post $post
     *
     * @return Tag
     */
    public function addPost(Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove Post
     *
     * @param BetaMFD\WikiBundle\Entity\Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

}
