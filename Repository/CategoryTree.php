<?php

namespace BetaMFD\WikiBundle\Repository;

/**
 * Because of hos objects persist,
 * a tree is easier to build as an object than an array
 */
class CategoryTree
{
    private $children;
    private $category;

    public function __construct(\BetaMFD\WikiBundle\Entity\PostCategory $category)
    {
        $this->category = $category;
    }

    /**
     * Get the value of Children
     *
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function addChild(CategoryTree $child)
    {
        //prevent me from adding ME as a child
        $index = $child->getCategory()->getCategory();
        $this->children[$index] = $child;
        return $this;
    }

    /**
     * Get the value of Category
     *
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of Category
     *
     * @param mixed category
     *
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

}
