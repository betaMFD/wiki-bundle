

This bundle is VERY WIP. It's not really usable.




Composer install directions deliberately left out. Will add in later when this could be useful to someone else. In the mean time the following instructions are mostly so I don't forget steps when hooking it up.



### Enable the Bundle (Symfony 2 or 3 without flex)

Enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new BetaMFD\WikiBundle\BetaMFDWikiBundle(),
        );

        // ...
    }

    // ...
}
```

### Connect UserInterface in config.yml
Replace 'App\Entity\User' with whatever the path is to your User entity.

```
    orm:
        resolve_target_entities:
            BetaMFD\WikiBundle\Model\UserInterface: App\Entity\User
```

### Map to Doctrine Entity Manager
I use multiple entity managers in one project. Before updating the database will work, I need to tell it what manager to use.

```
    orm:
        entity_managers:
            default:
                mappings:
                    BetaMFDWikiBundle: ~
```

### Update Database
Create migrations or update your database for the new tables that this bundle brings in.


### Add routing
```
#app/config/routing.yml
wiki:
  resource: "@BetaMFDWikiBundle/resources/config/routing.xml"
  prefix: "/wiki"
```


### Override any Template

1. Create `App/Resources/BetaMFDWikiBundle/views`
1. Create any template file you want to override in that folder
