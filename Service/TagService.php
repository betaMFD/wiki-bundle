<?php

namespace BetaMFD\WikiBundle\Service;


use BetaMFD\WikiBundle\Entity\Tag;

/**
 * This service takes care of adding/removing tags to entities
 *
 *  To handle the tag data you need to inject this service into your controller:
 *    private $tagService;
 *    public function __construct(\App\Service\TagService $tagService)
 *    {
 *        $this->tagService = $tagService;
 *    }
 *
 *  When you handle the form, use this service to handle the tag list:
 *     $tags = $form['tag_list']->getData();
 *     $this->tagService->setTagsFromString($tags);
 *     $this->tagService->addTagsToEntity($item);
 *
 */
class TagService
{
    private $em;
    private $tags;

    public function __construct(
        \Doctrine\ORM\EntityManagerInterface $entityManager
        )
    {
        $this->em = $entityManager;
    }

    public function setTagsFromString($string_of_tags)
    {
        if (!empty($string_of_tags)) {
            //remove unnecessary spaces after commas so I can just replace commas
            $string = str_replace(', ', ',', $string_of_tags);
            //explode on commas
            $tags = explode(',', $string);
            $this->tags = $tags;
        }
        return $this;
    }

    public function addTagsToEntity($entity, $string_of_tags = null, $clear = true)
    {
        if (!empty($string_of_tags)) {
            $this->setTagsFromString($string_of_tags);
        }
        //get entity and clear out the tags from it
        $this->em->persist($entity);
        $entity->clearTags();
        if (empty($this->tags)){
            return $this;
        }
        foreach ($this->tags as $tag) {
            if (!empty($tag)) {
                $entity_tag = $this->em->getRepository('BetaMFDWikiBundle:Tag')->find($tag);
                if (empty($entity_tag)) {
                    $entity_tag = new Tag($tag);
                    $this->em->persist($entity_tag);
                }
                $entity->addTag($entity_tag);
            }
        }
        return $this;
    }


}
