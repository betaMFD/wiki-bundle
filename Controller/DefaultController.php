<?php

namespace BetaMFD\WikiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@BetaMFDWiki/base.html.twig');
    }
}
