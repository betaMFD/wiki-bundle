<?php

namespace BetaMFD\WikiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BetaMFD\WikiBundle\Form\CategoryType;

class CategoryController extends Controller
{
    public function indexAction()
    {
        $tree = $this->getDoctrine()->getManager()->getRepository('BetaMFDWikiBundle:PostCategory')->findTree();
        return $this->render('@BetaMFDWiki/category/index.html.twig', ['tree' => $tree]);
    }

}
