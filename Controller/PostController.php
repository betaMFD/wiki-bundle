<?php

namespace BetaMFD\WikiBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use BetaMFD\WikiBundle\Entity\Post;
use BetaMFD\WikiBundle\Entity\PostType;
use BetaMFD\WikiBundle\Entity\PostVersion;
use BetaMFD\WikiBundle\Utils\Functions;

/**
 * @Route("/wiki/post", name="wiki_post_")
 */
class PostController extends Controller
{
    private $tagService;
    private $em;

    public function __construct(
        \BetaMFD\WikiBundle\Service\TagService $tagService,
        EntityManagerInterface $entityManager
    ) {
        $this->tagService = $tagService;
        $this->em = $entityManager;
    }

    /**
     * @Route("/{type}/edit/{id}",
     *     name="edit",
     *     defaults={"type": "wiki", "id": null}
     *   )
     * @ParamConverter("type",
     *     options={
     *         "mapping": {"type" = "type"}
     *         })
     */
    public function editAction(Request $request, Post $post = null, PostType $type)
    {
        if (empty($post)) {
            $return['post'] = new Post();
            $return['post']->setCurrentVersion(new PostVersion());
            $return['post']->setType($type);
        } else {
            $return['post'] = $post;
        }
        $return['type'] = $type;
        $tree = $this->em->getRepository('BetaMFDWikiBundle:PostCategory')->findTree();
        return $this->render('@BetaMFDWiki/post/edit.html.twig', $return);
    }

    /**
    * @Route("/{type}/submit/{id}",
    *     name="submit",
    *     defaults={"type": "wiki", "id": null}
    *   )
    * @ParamConverter("type",
     *     options={
     *         "mapping": {"type" = "type"}
     *         })
    */
    public function submitAction(Request $request, Post $post = null, PostType $type)
    {
        $submitted = $request->request->all();
        $title = $request->get('_title');
        $slug = $request->get('_slug');
        $comment = $request->get('_comment');
        $tagString = $request->get('_tags');
        $draft = $request->get('_draft');
        $html = $request->get('_html');
        $markup = $request->get('_markup');
        $version = new PostVersion();
        if (empty($post)) {
            //new posts are easiest
            $post = new Post();
            $this->em->persist($post);
            //set type based on URL
            if (empty($type)) {
                //Should I just create a new type?? Or . . . ?
                return new Response(json_encode([
                    'status'=>'fail',
                    'reason' => 'The given type was not found. This error shouldn\'t be possible',
                ]), 404);
            }
            $post->setType($type);
        }

        $this->em->persist($version);
        switch($request->request->get('_version')) {
            case "major":
            $post->setNewMajorVersion($version);
            break;
            case "minor":
            $post->setNewMinorVersion($version);
            break;
            case "patch":
            $post->setNewPatchVersion($version);
            break;
            default:
            //this should probably not be possible.
            return new Response(json_encode([
                'status'=>'fail',
                'reason' => 'Version type was not chosen. Please choose a version.',
            ]), 400);
        }

        $post->setTitle($title);
        $post->setSlug($slug);
        $post->setComment($comment);
        $post->setDraft(!empty($draft) ? true : false);
        $post->setHtml($html);
        $post->setMarkup($markup);
        $post->setUser($this->getUser());
        //set tags using the tag service
        $this->tagService->addTagsToEntity($post, $tagString);

        $this->em->flush();
        return new Response(json_encode([
            'status'=>'success',
        ]));
    }


    /**
     * @Route("/{type}/all", name="all_by_type",
     *     defaults={"type": "wiki"}
     *   )
     * @ParamConverter("type",
      *     options={
      *         "mapping": {"type" = "type"}
      *         })
     */
    public function allTypeAction(PostType $type)
    {
        $posts = $this->em->getRepository('BetaMFDWikiBundle:Post')->findByType($type);

        //reorganize for better display
        foreach ($posts as $post) {
            $category = $post->getCategory();
            $cat = (empty($category)) ? 0 : $category->getCategory();
            $title = $post->getTitle();
            $list[$cat][$title] = $post;
        }
        Functions::ksortRecursive($list);

        $return['type'] = $type;
        $return['list'] = $list;
        return $this->render('@BetaMFDWiki/post/list.html.twig', $return);
    }

    /**
     * @Route("/slug/{slug}", name="slug_test")
     */
    public function slugTestAction($slug = null)
    {
        $post = $this->em->getRepository('BetaMFDWikiBundle:Post')->findOneBySlug($slug);
        if (empty($post)) {
            return new Response(json_encode([
                'status' => 'success',
            ]));
        } else {
            return new Response(json_encode([
                'message' => 'Slug already exists.',
            ]), 409);
        }
    }

    /**
     * @Route("/{slug}",
     *     name="view",
     *     defaults = {"slug": null}
     *     )
     * @ParamConverter("slug",
     *     options={
     *         "mapping": {"slug" = "slug"}
     *         })
    */
    public function viewAction(Post $post)
    {
        return $this->render('@BetaMFDWiki/post/view.html.twig', ['post' => $post]);
    }
}
