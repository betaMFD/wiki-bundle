<?php

namespace BetaMFD\WikiBundle\Model;


interface UserInterface
{

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId();

    public function getName();
}
