<?php

/**
 * @author Meghan Doyle
 * @copyright 2016
 */

namespace BetaMFD\WikiBundle\Utils;

class Functions
{
    public static function ksortRecursive(&$array, $sort_flags = SORT_REGULAR)
    {
        //https://gist.github.com/cdzombak/601849
        if (!is_array($array)) {
            return false;
        }
        ksort($array, $sort_flags);
        foreach ($array as &$arr) {
            self::ksortRecursive($arr, $sort_flags);
        }
        return true;
    }
}
